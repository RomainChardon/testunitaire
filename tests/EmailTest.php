<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    // Verif si l'email est valid c
    public function testCanBeCreatedFromValidEmailAddress(): void
    {
        $this->assertInstanceOf(
            Email::class,
            Email::fromString('user@example.com')
        );
    }

    // verif que l'email invalide creer une erreur
    public function testCannotBeCreatedFromInvalidEmailAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Email::fromString('invalid');
    }

    // Verif renvoi l'email en string
    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals(
            'user@example.com',
            Email::fromString('user@example.com')
        );
    }
}
