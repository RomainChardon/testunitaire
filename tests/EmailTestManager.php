public function testCorrectAccessToDB(): void {
    // ce test vérifie que l’appel à la méthode getDB() de database renvoie bien un objet de type PDO
}

public function testCanInsertNewMail(): void {
    //Cette méthode doit créer une instance de managerEmail et une instance d’email, puis vérifier que le
    résultat de l’appel de la méthode insert du manager renvoie 2 (on a un auto incrément, et le premier mail est à
    1)
}

public function testCannotInsertSameMail(): void {
    //Cette méthode doit insérer le même mail que dans le test précédent, ce qui doit lever une exception de
    type Exception car les mails doivent être uniques en BDD. On vérifie que l’erreur se lance bien
}