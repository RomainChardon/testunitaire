-- Base de données : `phpUnitMail`
CREATE DATABASE IF NOT EXISTS `phpUnitMail` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `phpUnitMail`;

-- Structure de la table `mail`
CREATE TABLE `mail` (
`id` int(11) NOT NULL,
`mail` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Contenu de la table `mail`
INSERT INTO `mail` (`id`, `mail`) VALUES
(1, 'test@test.fr');

-- Index pour la table `mail`
ALTER TABLE `mail`
ADD PRIMARY KEY (`id`);
ALTER TABLE `mail` ADD UNIQUE(`mail`);

-- AUTO_INCREMENT pour la table `mail` s
ALTER TABLE `mail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;