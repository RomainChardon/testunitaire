<?php
require_once ("database.php");

class EmailManager {

    private $db;

    public function __construct($database){
        $this->db=$database;
    }

    public function insert(Email $mail){
        $query = "insert into `mail` (`id`, `mail`) values (?,?);";
        $traitement = $this->db->prepare($query);
        $param1=$mail->getId();
        $traitement->bindparam(1,$param1);
        $param2=$mail->getMail();
        $traitement->bindparam(2,$param2);
        $res = $traitement->execute();
        if(!$res){
            throw new Exception(
                sprintf(
                    '"%s" cannot be inserted in database',
                    $mail->getMail()
                )
            );
        }
    $mail->setId($this->db->lastInsertId());
    return $mail->getId();
    }
}